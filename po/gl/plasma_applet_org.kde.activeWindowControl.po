# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Adrian Chaves <adrian@chaves.io>, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2021-05-23 00:18+0000\n"
"PO-Revision-Date: 2019-10-27 07:59+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: package/contents/config/config.qml:6
msgid "Appearance"
msgstr "Aparencia"

#: package/contents/config/config.qml:11
msgid "Behaviour"
msgstr "Comportamento"

#: package/contents/config/config.qml:16
msgid "Buttons"
msgstr "Botóns"

#: package/contents/config/config.qml:21
msgid "Mouse Control"
msgstr "Control do rato"

#: package/contents/config/config.qml:26
msgid "Application Menu"
msgstr "Menú de aplicacións"

#: package/contents/ui/config/ConfigAppearance.qml:73
msgctxt "Use default font"
msgid "Default"
msgstr "Predeterminado"

#: package/contents/ui/config/ConfigAppearance.qml:90
msgid "Plasmoid version: "
msgstr "Versión do plasmoide: "

#: package/contents/ui/config/ConfigAppearance.qml:96
msgid "Width in horizontal panel:"
msgstr "Anchura no panel horizontal:"

#: package/contents/ui/config/ConfigAppearance.qml:103
msgid "Fill width"
msgstr "Encher a anchura"

#: package/contents/ui/config/ConfigAppearance.qml:122
msgid "Fine tuning:"
msgstr "Axuste fino:"

#: package/contents/ui/config/ConfigAppearance.qml:142
msgid "Hide titlebar for maximized windows (takes effect immediately)"
msgstr ""
"Agochar a barra de título para as xanelas maximizadas (fai efecto "
"inmediatamente)"

#: package/contents/ui/config/ConfigAppearance.qml:158
msgid "Show window title"
msgstr "Mostrar os títulos das xanelas"

#: package/contents/ui/config/ConfigAppearance.qml:173
msgid "Text type:"
msgstr "Tipo de texto:"

#: package/contents/ui/config/ConfigAppearance.qml:178
#: package/contents/ui/config/ConfigAppearance.qml:196
#: package/contents/ui/config/ConfigAppearance.qml:201
msgid "Window title"
msgstr "Título da xanela"

#: package/contents/ui/config/ConfigAppearance.qml:178
#: package/contents/ui/config/ConfigAppearance.qml:196
msgid "Application name"
msgstr "Nome da aplicación"

#: package/contents/ui/config/ConfigAppearance.qml:182
msgid "Fit text:"
msgstr "Axustar o texto:"

#: package/contents/ui/config/ConfigAppearance.qml:187
msgid "Just elide"
msgstr "Simplemente suprimir"

#: package/contents/ui/config/ConfigAppearance.qml:187
msgid "Fit on hover"
msgstr "Axustar ao pasar por riba"

#: package/contents/ui/config/ConfigAppearance.qml:187
msgid "Always fit"
msgstr "Axustar sempre"

#: package/contents/ui/config/ConfigAppearance.qml:191
msgid "Tooltip text:"
msgstr "Texto do consello:"

#: package/contents/ui/config/ConfigAppearance.qml:196
msgid "No tooltip"
msgstr "Sen consello"

#: package/contents/ui/config/ConfigAppearance.qml:233
msgid "No window text:"
msgstr "Sen texto de xanela:"

#: package/contents/ui/config/ConfigAppearance.qml:247
msgid "Use %activity% placeholder to show current activity name."
msgstr ""
"Use a marca de substitución %activity% para mostrar o nome da actividade "
"actual."

#: package/contents/ui/config/ConfigAppearance.qml:254
msgid "No window icon:"
msgstr "Sen icona de xanela:"

#: package/contents/ui/config/ConfigAppearance.qml:265
msgid "Limit text width"
msgstr "Limitar a anchura do texto"

#: package/contents/ui/config/ConfigAppearance.qml:275
#: package/contents/ui/config/ConfigAppMenu.qml:95
msgctxt "Abbreviation for pixels"
msgid "px"
msgstr "px"

#: package/contents/ui/config/ConfigAppearance.qml:288
msgid "Show window icon"
msgstr "Mostrar a icona da xanela"

#: package/contents/ui/config/ConfigAppearance.qml:294
msgid "Window icon on the right"
msgstr "Icona da xanela na dereita"

#: package/contents/ui/config/ConfigAppearance.qml:315
msgid "Bold text"
msgstr "Texto en letra grosa"

#: package/contents/ui/config/ConfigAppearance.qml:319
msgid "Text font:"
msgstr "Tipo de letra do texto:"

#: package/contents/ui/config/ConfigAppearance.qml:340
msgid "Icon and text spacing:"
msgstr "Espazo de icona e texto:"

#: package/contents/ui/config/ConfigAppearance.qml:352
msgid "Font size scale:"
msgstr "Escala do tamaño do tipo de letra:"

#: package/contents/ui/config/ConfigAppMenu.qml:23
msgid "Enable application menu"
msgstr "Activar o menú da aplicación"

#: package/contents/ui/config/ConfigAppMenu.qml:38
msgid "Fill height"
msgstr "Encher a altura"

#: package/contents/ui/config/ConfigAppMenu.qml:44
msgid "Bold font"
msgstr "Letra grosa"

#: package/contents/ui/config/ConfigAppMenu.qml:50
#: package/contents/ui/config/ConfigButtons.qml:173
msgid "Do not hide on mouse out"
msgstr "Non ocultar ao saír de enriba"

#: package/contents/ui/config/ConfigAppMenu.qml:55
msgid "Show next to buttons"
msgstr "Mostrar canda os botóns"

#: package/contents/ui/config/ConfigAppMenu.qml:61
msgid "Show next to icon and text"
msgstr "Mostrar canda a icona e o texto"

#: package/contents/ui/config/ConfigAppMenu.qml:67
msgid "Switch sides with icon and text"
msgstr "Cambiar de lado coa icona e o texto"

#: package/contents/ui/config/ConfigAppMenu.qml:74
msgid "Show separator"
msgstr "Mostrar un separador"

#: package/contents/ui/config/ConfigAppMenu.qml:81
msgid "Make window title bold when menu is displayed"
msgstr "Usar letra grosa para o título ao mostrar o menú"

#: package/contents/ui/config/ConfigAppMenu.qml:86
msgid "Side margin:"
msgstr "Marxe lateral:"

#: package/contents/ui/config/ConfigAppMenu.qml:99
msgid "Icon and text opacity:"
msgstr "Opacidade da icona e o texto:"

#: package/contents/ui/config/ConfigAppMenu.qml:111
msgid "Menu button text size scale:"
msgstr "Escala de tamaño do texto do botón do menú:"

#: package/contents/ui/config/ConfigBehaviour.qml:14
msgid "Show active window only for plasmoid's screen"
msgstr "Mostrar a xanela activa só para a pantalla do plasmoide"

#: package/contents/ui/config/ConfigButtons.qml:90
msgid "Enable Control Buttons"
msgstr "Activar os botóns de control"

#: package/contents/ui/config/ConfigButtons.qml:111
msgid "Show minimize button"
msgstr "Mostrar o botón de minimizar"

#: package/contents/ui/config/ConfigButtons.qml:116
msgid "Show maximize button"
msgstr "Mostrar o botón de maximizar"

#: package/contents/ui/config/ConfigButtons.qml:121
msgid "Show pin to all desktops"
msgstr "Mostrar ancorar en todos os escritorios"

#: package/contents/ui/config/ConfigButtons.qml:131
msgid "Button order:"
msgstr "Orde dos botóns:"

#: package/contents/ui/config/ConfigButtons.qml:167
msgid "Behaviour:"
msgstr "Comportamento:"

#: package/contents/ui/config/ConfigButtons.qml:184
msgid "Show only when maximized"
msgstr "Mostrar só ao estar maximizada"

#: package/contents/ui/config/ConfigButtons.qml:189
msgid "Buttons next to icon and text"
msgstr "Botóns canda a icona e o texto"

#: package/contents/ui/config/ConfigButtons.qml:194
msgid "Buttons between icon and text"
msgstr "Botóns entre a icona e o texto"

#: package/contents/ui/config/ConfigButtons.qml:200
msgid "Dynamic width"
msgstr "Anchura dinámica"

#: package/contents/ui/config/ConfigButtons.qml:206
msgid "Sliding icon and text"
msgstr "Icona e texto corredíos"

#: package/contents/ui/config/ConfigButtons.qml:218
msgid "Position:"
msgstr "Posición:"

#: package/contents/ui/config/ConfigButtons.qml:224
msgid "Upper left"
msgstr "Superior esquerda"

#: package/contents/ui/config/ConfigButtons.qml:235
msgid "Upper right"
msgstr "Superior dereita"

#: package/contents/ui/config/ConfigButtons.qml:241
msgid "Bottom left"
msgstr "Inferior esquerda"

#: package/contents/ui/config/ConfigButtons.qml:247
msgid "Bottom right"
msgstr "Inferior dereita"

#: package/contents/ui/config/ConfigButtons.qml:253
msgid "Vertical center"
msgstr "Centrado vertical"

#: package/contents/ui/config/ConfigButtons.qml:263
msgid "Button size:"
msgstr "Tamaño dos botóns:"

#: package/contents/ui/config/ConfigButtons.qml:275
msgid "Buttons spacing:"
msgstr "Espazo entre os botóns:"

#: package/contents/ui/config/ConfigButtons.qml:294
msgid "Theme"
msgstr "Tema"

#: package/contents/ui/config/ConfigButtons.qml:300
msgid "Automatic"
msgstr "Automático"

#: package/contents/ui/config/ConfigButtons.qml:304
msgid "Absolute path to aurorae button theme folder"
msgstr "Ruta absoluta do cartafol de tema de botóns de aurorae"

#: package/contents/ui/config/ConfigMouseControl.qml:84
msgid "Mouse Buttons:"
msgstr "Botóns do rato:"

#: package/contents/ui/config/ConfigMouseControl.qml:89
msgid "Doubleclick to toggle maximizing"
msgstr "Clic duplo para conmutar a maximización"

#: package/contents/ui/config/ConfigMouseControl.qml:101
msgid "Left click disabled"
msgstr "Clic principal desactivado"

#: package/contents/ui/config/ConfigMouseControl.qml:108
msgid "Left click to present windows (Current Desktop)"
msgstr "Clic principal para presentar as xanelas (escritorio actual)"

#: package/contents/ui/config/ConfigMouseControl.qml:115
msgid "Left click to present windows (All Desktops)"
msgstr "Clic principal para presentar as xanelas (todos os escritorios)"

#: package/contents/ui/config/ConfigMouseControl.qml:122
msgid "Left click to present windows (Window Class)"
msgstr "Clic principal para presentar as xanelas (clase de xanela)"

#: package/contents/ui/config/ConfigMouseControl.qml:140
msgid "Middle click disabled"
msgstr "Clic central desactivado"

#: package/contents/ui/config/ConfigMouseControl.qml:146
msgid "Middle click to close active window"
msgstr "Clic centrar para pechar a xanela activa"

#: package/contents/ui/config/ConfigMouseControl.qml:152
msgid "Middle click to toggle fullscreen"
msgstr "Clic centrar para conmutar a pantalla completa"

#: package/contents/ui/config/ConfigMouseControl.qml:163
msgid "Mouse Wheel:"
msgstr "Roda do rato:"

#: package/contents/ui/config/ConfigMouseControl.qml:168
msgid "Mouse wheel up to maximize"
msgstr "Roda do rato cara arriba para maximizar"

#: package/contents/ui/config/ConfigMouseControl.qml:178
msgid "Mouse wheel down disabled"
msgstr "Roda do rato cara abaixo desactivada"

#: package/contents/ui/config/ConfigMouseControl.qml:184
msgid "Mouse wheel down to minimize"
msgstr "Roda do rato cara abaixo para minimizar"

#: package/contents/ui/config/ConfigMouseControl.qml:190
msgid "Mouse wheel down to unmaximize"
msgstr "Rota do rato cara abaixo para deixar de maximizar"

#: package/contents/ui/config/IconPicker.qml:73
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose..."
msgstr "Escoller…"

#: package/contents/ui/config/IconPicker.qml:78
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr "Borrar a icona"

#: package/contents/ui/main.qml:583
msgid "Switch to activity: %1"
msgstr "Cambiar á actividade: %1"

#: package/contents/ui/main.qml:591
msgid "Close"
msgstr "Pechar"

#: package/contents/ui/main.qml:592
msgid "Toggle Maximise"
msgstr "Conmutar maximizar"

#: package/contents/ui/main.qml:593
msgid "Minimise"
msgstr "Minimizar"

#: package/contents/ui/main.qml:594
msgid "Toggle Pin To All Desktops"
msgstr "Conmutar ancorar en todos os escritorios"

#: package/contents/ui/main.qml:596
msgid "Reload Theme"
msgstr "Cargar de novo o tema"
